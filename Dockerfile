### ############
## build web
### ############
FROM node:latest

RUN node --version
RUN npm --version

WORKDIR /code

COPY . /code
COPY ./templates/ /code/templates

# workaround... should be removed asap
#RUN rm package-lock.json
#RUN npm install
# --------------------------------

RUN npm ci
RUN npm run build:prod

ENTRYPOINT [ "/bin/bash" ]

### ############
## test & build
### ############
FROM clux/muslrust:nightly

ENV RUSTFLAGS="-Clinker=musl-gcc"
ENV RUST_BACKTRACE="full"

RUN cargo --version

WORKDIR /code

RUN cargo init
COPY Cargo.toml /code/Cargo.toml
RUN cargo fetch
COPY src /code/src
COPY --from=0 /code/web/dist /code/templates
RUN cargo fetch

# TODO: #RUN cargo test --offline
# TODO: #RUN cargo bench --offline
RUN cargo build --release --offline

RUN cp /code/target/x86_64-unknown-linux-musl/release/whoip /code/whoip

### ############
## make image
### ############
#FROM debian:latest
FROM alpine:latest

ENV CARGO_MANIFEST_DIR "/app"
ENV RUN_MODE "development"
ENV TZ "Etc/UTC"
ENV RUST_BACKTRACE "full"

ENV APP_WEB_FILES_DIR "/app/templates"
ENV APP_LOG_LEVEL "DEBUG"
ENV APP_SERVER_WORKERS "1"
ENV APP_SERVER_HOST "0.0.0.0"
ENV APP_SERVER_PORT "1337"

WORKDIR /app

COPY ./static /app/static
COPY --from=0 /code/web/dist /app/templates
COPY --from=1 /code/whoip /app/whoip
RUN ln -s /app/whoip /usr/bin/whoip

EXPOSE 1337

ENTRYPOINT [ "/usr/bin/whoip" ]
#ENTRYPOINT [ "/bin/bash" ]
