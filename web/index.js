import "./style.scss";
import Inputmask from "inputmask";
import { log, LogLevel } from "missionlog";

/**
 * initialize missionlog
 * @param config JSON which assigns tags levels. An uninitialized,
 *    tag's level defaults to INFO.
 * @param callback? handle logging whichever way works best for you
 */
log.init(
  { transporter: "INFO", security: "ERROR", system: "INFO" },
  (level, tag, msg, params) => {
    const prefix = `${level}: [${tag}] `;
    switch (level) {
      case LogLevel.ERROR:
        // eslint-disable-next-line no-console
        console.error(prefix, msg, ...params);
        break;
      case LogLevel.WARN:
        // eslint-disable-next-line no-console
        console.warn(prefix, msg, ...params);
        break;
      case LogLevel.INFO:
        // eslint-disable-next-line no-console
        console.info(prefix, msg, ...params);
        break;
    }
  });

if (document.readyState !== "loading") {
  // log.info(tag.debug, "document is already ready, just execute code here");
  init();
} else {
  document.addEventListener("DOMContentLoaded", function () {
    // log.info(tag.debug, "document was not ready, place code here");
    init();
  });
}

function init() {
  const doUpdate = () => {
    // log.info(tag.debug, "updating...");
  };

  Inputmask({
    regex: "^(?:(?:25[0-5]|2[0-4][0-9]|" +
      "[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|" +
      "2[0-4][0-9]|" +
      "[01]?[0-9][0-9]?)$"
  }).mask(document.querySelectorAll(".masked-ip-v4"));

  Inputmask({
    regex: "(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|" +
      "([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|" +
      "([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|" +
      "([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|" +
      "([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|" +
      "([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|" +
      "[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|" +
      "fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|" +
      "::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|" +
      "(2[0-4]|1{0,1}[0-9]){0,1}[0-9]).){3,3}(25[0-5]|" +
      "(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|" +
      "(2[0-4]|1{0,1}[0-9]){0,1}[0-9]).){3,3}(25[0-5]|" +
      "(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))"
  }).mask(document.querySelectorAll(".masked-ip-v6"));

  // eslint-disable-next-line no-unused-vars
  const setClipboard = (value) => {
    const tempInput = document.createElement("input");
    tempInput.style.position = "absolute";
    tempInput.style.left = "-1000px";
    tempInput.style.top = "-1000px";
    tempInput.value = value;
    document.body.appendChild(tempInput);
    tempInput.select();
    document.execCommand("copy");
    document.body.removeChild(tempInput);
  };

  document.querySelectorAll(".default-toast")
    .forEach((inputElement) => {
      inputElement.classList.add("show");
    });

  document.querySelectorAll(".copy-to-clipboard-button")
    .forEach((inputElement) => {
      inputElement.addEventListener("click", () => {
        setClipboard(inputElement.dataset.value);
      });
    });

  document.querySelectorAll("#headerContainer a")
    .forEach((inputElement) => {
      inputElement.addEventListener("input", () => {
        doUpdate();
      });
    });

  const informationContainer = document.querySelector("#informationContainer");
  // const headerContainer = document.querySelector("#headerContainer");

  // document.querySelectorAll(".icon-only-button")
  //   .forEach((inputElement) => {
  //     inputElement.addEventListener("mouseenter", () => {
  // let logger = Logger::default();
  //       inputElement.querySelectorAll(".icon-only-button-label")
  //         .forEach((inputElement) => {
  //           inputElement.classList.remove("hidden");
  //         });
  //     });
  //     inputElement.addEventListener("mouseleave", () => {
  //       inputElement.querySelectorAll(".icon-only-button-label")
  //         .forEach((inputElement) => {
  //           inputElement.classList.add("hidden");
  //         });
  //     });
  //   });

  informationContainer.addEventListener("mouseenter", () => {
    // log.info(tag.debug, "mouseenter");
    informationContainer.querySelectorAll(".icon-only-button")
      .forEach((inputElement) => {
        // log.info(tag.debug, ">inputElement: ", inputElement);
        inputElement.classList.replace("icon-only-button-default", "icon-only-button-hover");
      });
  });

  informationContainer.addEventListener("mouseleave", () => {
    // log.info(tag.debug, "mouseleave");
    informationContainer.querySelectorAll(".icon-only-button")
      .forEach((inputElement) => {
        // log.info(tag.debug, ">inputElement: ", inputElement);
        inputElement.classList.replace("icon-only-button-hover", "icon-only-button-default");
      });
  });

  doUpdate();
}
