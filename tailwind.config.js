const Colorkraken = require("colorkraken");
const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  purge: [
    "./src/**/*.html",
    "./src/**/*.js"
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        sans: ["Fira Sans", ...defaultTheme.fontFamily.sans]
      },
      colors: Colorkraken,
      textColors: Colorkraken,
      backgroundColors: Colorkraken
    }
  },
  variants: {
    extend: {
      opacity: ["disabled"]
    }
  },
  plugins: [
    require("@tailwindcss/forms"),
    require("@tailwindcss/typography")
  ]
};
