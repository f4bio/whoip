use std::env;

use config::{Config, ConfigError, Environment, File};
use dotenv::dotenv;
use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Log {
  pub actix_web: String,
  pub whoip: String,
}

#[derive(Debug, Deserialize)]
pub struct Server {
  pub workers: u8,
  pub host: String,
  pub port: u16
}

#[derive(Debug, Deserialize)]
pub struct Settings {
  pub debug: bool,
  pub log: Log,
  pub server: Server,
}

impl Settings {
  pub fn new() -> Result<Self, ConfigError> {
    dotenv().ok();

    let mut s = Config::new();

    // Start off by merging in the "default" configuration file
    s.merge(File::with_name(
      concat!(env!("CARGO_MANIFEST_DIR"), "/config/default"))
    )?;

    // Add in the current environment file
    // Default to 'development' env
    // Note that this file is _optional_
    let env = env::var("RUN_MODE").unwrap_or_else(|_| "development".into());
    s.merge(File::with_name(
      &format!(concat!(env!("CARGO_MANIFEST_DIR"), "/config/{}"), env)
    ).required(false))?;

    // Add in a local configuration file
    // This file shouldn't be checked in to git
    s.merge(File::with_name(
      concat!(env!("CARGO_MANIFEST_DIR"), "/config/local")
    ).required(false))?;

    // Add in settings from the environment (with a prefix of APP)
    // Eg.. `APP_DEBUG=1 ./target/app` would set the `debug` key
    s.merge(Environment::with_prefix("APP"))?;

    // You may also programmatically change settings
    // s.set("database.url", "postgres://")?;

    // Now that we're done, let's access our configuration
    // println!("debug: {:?}", s.get_bool("debug"));

    // You can deserialize (and thus freeze) the entire configuration as
    s.try_into()
  }
}
