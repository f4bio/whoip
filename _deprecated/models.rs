use std::fmt;
use std::net::IpAddr;
use std::path::Path;
use std::str::FromStr;

use anyhow::Error;
use language_tags::LanguageTag;
use log::{debug, error};
use maxminddb::{geoip2, MaxMindDBError, Reader};
use maxminddb::geoip2::{City, Isp};
use maxminddb::geoip2::model::Postal;
use serde::{Deserialize, Serialize};

lazy_static! {
  static ref MAXMINDDB_ASN: Reader<Vec<u8>> = Reader::open_readfile(
    Path::new(env!("CARGO_MANIFEST_DIR"))
    .join("static").join("geoipdb").join("GeoLite2-ASN.mmdb")
  ).unwrap();
  static ref MAXMINDDB_CITY: Reader<Vec<u8>> = Reader::open_readfile(
    Path::new(env!("CARGO_MANIFEST_DIR"))
    .join("static").join("geoipdb").join("GeoLite2-City.mmdb")
  ).unwrap();
  static ref MAXMINDDB_COUNTRY: Reader<Vec<u8>> = Reader::open_readfile(
    Path::new(env!("CARGO_MANIFEST_DIR"))
    .join("static").join("geoipdb").join("GeoLite2-Country.mmdb")
  ).unwrap();
}

#[derive(Debug, Serialize, Deserialize)]
pub struct IpQuery {
  pub ip: Option<String>,
}

/// CountryInfo
#[derive(Debug, Serialize, Deserialize)]
pub struct CountryInfo {
  pub name: String,
  pub code: String,
}

impl CountryInfo {
  pub fn new(ip: &IpAddr, user_lang: &LanguageTag) -> Self {
    let geoip: geoip2::Country = MAXMINDDB_CITY.lookup(*ip).unwrap();

    CountryInfo {
      name: geoip.clone().country.unwrap().names.as_ref().unwrap()
        .get(user_lang.primary_language()).unwrap()
        .to_string(),
      code: geoip.clone().country.unwrap()
        .iso_code.unwrap()
        .to_lowercase(),
    }
  }
}

/// CityInfo
#[derive(Debug, Serialize, Deserialize)]
pub struct CityInfo {
  pub name: String,
  pub code: String,
}

impl CityInfo {
  pub fn new(ip: &IpAddr, user_lang: &LanguageTag) -> Result<Self, Error> {
    // println!("CityInfo->new(ip): {}", ip);

    let geoip: Result<City, MaxMindDBError> = MAXMINDDB_CITY.lookup(*ip);
    let lang_tag: &str = user_lang.primary_language();

    // println!("CityInfo->new(geoip): {:?}", geoip);

    return if geoip.is_ok() {
      let g = geoip.unwrap().clone();
      // println!("CityInfo->new(g): {:?}", g);

      let city = g.city.unwrap();
      // println!("CityInfo->new(city): {:?}", city);

      let postal = g.postal.unwrap_or(Postal { code: Some("") });
      // println!("CityInfo->new(postal): {:?}", postal);

      let city_name = city.names.unwrap().get(&lang_tag).unwrap().to_string();
      // println!("CityInfo->new(city_name): {:?}", city_name);

      let city_code = String::from(postal.code.unwrap_or(&"no city code"));
      // println!("CityInfo->new(city_code): {:?}", city_code);

      let res = CityInfo {
        name: city_name,
        code: city_code,
      };
      // println!("CityInfo->new(): {:?}", res);
      Ok(res)
    } else {
      let msg = format!("Cannot extract 'City' information from address '{}'!", ip);
      error!("CityInfo error: '{}'!", msg);

      return Err(Error::msg(msg))
    }
  }
}

/// LocationInfo
#[derive(Debug, Serialize, Deserialize)]
pub struct LocationInfo {
  pub latitude: f64,
  pub longitude: f64,
  pub metro_code: u16,
  pub time_zone: String,
}

impl LocationInfo {
  pub fn new(_ip: &IpAddr, _user_lang: &LanguageTag) -> Self {
    LocationInfo {
      latitude: 1.337,
      longitude: -1.337,
      metro_code: 1337,
      time_zone: String::from(""),
    }
  }
}

/// ConnectionInfo
#[derive(Debug, Serialize, Deserialize)]
pub struct ConnectionInfo {
  pub isp: Option<String>,
  // pub connection_type: String,
  // pub is_anonymous: bool,
  // pub is_anonymous_vpn: bool,
  // pub is_hosting_provider: bool,
  // pub is_public_proxy: bool,
  // pub is_tor_exit_node: bool,
}

impl ConnectionInfo {
  pub fn new(ip: &IpAddr, _user_lang: &LanguageTag) -> Result<Self, Error> {
    // println!("ConnectionInfo->new(ip): {}", ip);

    let geoip: Result<Isp, MaxMindDBError> = MAXMINDDB_ASN.lookup(*ip);
    // let lang_tag: &str = user_lang.primary_language();

    // println!("ConnectionInfo->new(geoip): {:?}", geoip);

    return if geoip.is_ok() {
      let g = geoip.unwrap().clone();
      println!("ConnectionInfo->new(g): {:?}", g);

      let isp = g.isp..unwrap_or(&"no isp");
      println!("ConnectionInfo->new(isp): {:?}", isp);

      if let Ok(x) =

      let res = ConnectionInfo {
        isp: if g.isp.is_some() {
          Ok(g.isp.unwrap())
        } else {
          Err()
        }
      };
      // println!("ConnectionInfo->new(): {:?}", res);
      Ok(res)
    } else {
      let msg = format!("Cannot extract 'Connection' information from address '{}'!", ip);
      error!("ConnectionInfo error: '{}'!", msg);

      return Err(Error::msg(msg))
    };
  }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct IpInfo {
  value: String,
  pub addr: Option<IpAddr>,
  pub language: Option<String>,
  pub country: Option<CountryInfo>,
  pub city: Option<CityInfo>,
  pub connection: Option<ConnectionInfo>,
  pub location: Option<LocationInfo>,
}

impl IpInfo {
  pub fn new(ip: &str) -> Self {
    IpInfo {
      value: String::from(ip),
      addr: None,
      language: None,
      country: None,
      city: None,
      connection: None,
      location: None,
    }
  }

  pub fn parse(ip: &str, user_lang: &LanguageTag) -> Result<Self, Error> {
    let parsed_addr = IpAddr::from_str(ip);
    return if parsed_addr.is_ok() {
      let addr = parsed_addr.unwrap();
      let geoip: Result<City, MaxMindDBError> = MAXMINDDB_CITY.lookup(addr);
      println!("IpInfo->parse(geoip): {:?}", geoip);

      let res = IpInfo {
        value: String::from(ip),
        addr: Some(addr),
        language: Some(user_lang.clone().into_string()),
        country: Some(CountryInfo::new(&addr, user_lang)),
        city: CityInfo::new(&addr, user_lang).ok(),
        connection: ConnectionInfo::new(&addr, user_lang).ok(),
        location: Some(LocationInfo::new(&addr, user_lang)),
      };
      println!("IpInfo: {:?}", res);
      Ok(res)
    } else {
      let msg = format!("Address '{}' cannot be parsed!", ip);
      error!("IpInfo parse error: '{}'!", msg);

      return Err(Error::msg(msg))
    }
  }
}

impl fmt::Display for IpInfo {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(f, "hello there! your IP is {}, you live in {}({}) and your language is {}",
           self.addr.as_ref().unwrap(),
           self.city.as_ref().unwrap().name,
           self.country.as_ref().unwrap().name,
           self.language.as_ref().unwrap(),
    )
  }
}
