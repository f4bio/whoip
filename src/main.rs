use std::collections::HashMap;
use std::env;
use std::net::IpAddr;
use std::path::Path;
use std::str::FromStr;

use actix_files as fs;
use actix_web::{App, Error, HttpRequest, HttpResponse, HttpServer, web};
use askama::Template;
use dotenv::dotenv;
use itconfig::*;
use tracing::{debug, info};
use tracing_actix_web::TracingLogger;
use tracing_bunyan_formatter::{BunyanFormattingLayer, JsonStorageLayer};
use tracing_log::LogTracer;
use tracing_subscriber::{EnvFilter, Registry};
use tracing_subscriber::layer::SubscriberExt;

use crate::common::{ExtractHeaders, ip_lookup};
use crate::models::{CityInfo, ConnectionInfo, CoordinatesInfo, CountryInfo, FlashMessage, IpInfoError, IpInfoObj, PostalInfo};

mod common;
mod models;

#[derive(Template)]
#[template(path = "index.html")]
struct IndexTemplate<'a> {
  message: Option<&'a FlashMessage>,
  language: &'a String,
  address: &'a String,
  country: Option<&'a CountryInfo>,
  city: Option<&'a CityInfo>,
  postal: Option<&'a PostalInfo>,
  coordinates: Option<&'a CoordinatesInfo>,
  connection: Option<&'a ConnectionInfo>,
}

/// text handler
async fn index_text(
  query: web::Query<HashMap<String, String>>,
  req: HttpRequest,
) -> Result<HttpResponse, Error> {

  info!("text: HttpRequest: {:?}", &req);

  let user_lang = req.extract_lang();
  let input_addr: IpAddr = if let Some(ip) = query.get("ip") {
    IpAddr::from_str(ip).unwrap()
  } else {
    req.extract_ip()
  };
  info!("text: user_lang: {:?} input_ip: {:?}",
        user_lang.primary_language(),
        input_addr
  );

  let mut content = String::from("");

  let resp: Result<IpInfoObj, IpInfoError> = ip_lookup(&input_addr);
  info!("text: IpInfoObj: {:?}", &resp);

  let infos = resp.unwrap();
  info!("text: infos: {:?}", &infos);

  let response_country;
  if infos.country.is_some() {
    response_country = infos.country.clone().unwrap();
    content = format!("Your IP is '{}' and your country is '{}'",
                      infos.address,
                      response_country.name);
  };

  Ok(
    HttpResponse::Ok()
      .content_type("text/plain")
      .body(content)
  )
}

/// json handler
async fn index_json(
  query: web::Query<HashMap<String, String>>,
  req: HttpRequest,
) -> Result<HttpResponse, Error> {
  info!("json: HttpRequest: {:?}", &req);

  let user_lang = req.extract_lang();
  let input_addr: IpAddr = if let Some(ip) = query.get("ip") {
    IpAddr::from_str(ip).unwrap()
  } else {
    req.extract_ip()
  };
  info!("json: user_lang: {:?} input_ip: {:?}",
        user_lang.primary_language(),
        input_addr
  );

  let resp: Result<IpInfoObj, IpInfoError> = ip_lookup(&input_addr);
  info!("json: IpInfoObj: {:?}", &resp);

  let infos = resp.unwrap();
  info!("json: infos: {:?}", &infos);

  Ok(
    HttpResponse::Ok()
    .content_type("application/json")
    .json(infos)
  )
}

/// web handler
async fn index_web(
  query: web::Query<HashMap<String, String>>,
  req: HttpRequest,
) -> Result<HttpResponse, Error> {
  info!("web: HttpRequest: {:?}", &req);

  let user_lang = req.extract_lang();
  let input_addr: IpAddr = if let Some(ip) = query.get("ip") {
    IpAddr::from_str(ip).unwrap()
  } else {
    req.extract_ip()
  };
  info!("web: user_lang: {:?} input_ip: {:?}",
        user_lang.primary_language(),
        input_addr
  );
  let resp: Result<IpInfoObj, IpInfoError> = ip_lookup(&input_addr);
  info!("web: IpInfoObj: {:?}", &resp);

  let infos = resp.unwrap();
  info!("web: infos: {:?}", &infos);

  let mut content = IndexTemplate {
    message: None,
    language: &String::from(user_lang.primary_language()),
    address: &infos.address,
    country: None,
    city: None,
    postal: None,
    coordinates: None,
    connection: None,
  };
  info!("web: content: {}", &content);

  let response_country;
  if infos.country.is_some() {
    response_country = infos.country.clone().unwrap();
    content.country = Some(&response_country)
  };

  let response_city;
  if infos.city.is_some() {
    response_city = infos.city.clone().unwrap();
    content.city = Some(&response_city);
  }

  let response_postal;
  if infos.postal.is_some() {
    response_postal = infos.postal.clone().unwrap();
    content.postal = Some(&response_postal);
  }

  let response_coordinates;
  if infos.coordinates.is_some() {
    response_coordinates = infos.coordinates.clone().unwrap();
    content.coordinates = Some(&response_coordinates);
  }

  let response_connection;
  if infos.connection.is_some() {
    response_connection  = infos.connection.clone().unwrap();
    content.connection = Some(&response_connection);
  }

  let response_body = content.render().unwrap();

  Ok(
    HttpResponse::Ok()
    .content_type("text/html")
    .body(response_body)
  )
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
  dotenv().ok();
  LogTracer::init().expect("Unable to setup log tracer!");
  // pretty_env_logger::init();

  let log_level: String = get_env_or_default("APP_LOG_LEVEL", "INFO");
  let app_name = concat!(env!("CARGO_PKG_NAME"), "-", env!("CARGO_PKG_VERSION")).to_string();
  let (non_blocking_writer, _guard) = tracing_appender::non_blocking(std::io::stdout());
  let bunyan_formatting_layer = BunyanFormattingLayer::new(app_name, non_blocking_writer);
  let subscriber = Registry::default()
    .with(EnvFilter::new(log_level.clone()))
    .with(JsonStorageLayer)
    .with(bunyan_formatting_layer);
  tracing::subscriber::set_global_default(subscriber).unwrap();

  debug!("Log level: '{}'", log_level.clone());
  info!("Creating app...");

  let app = move || {
    let app_web_files_dir = &env::var("APP_WEB_FILES_DIR").unwrap();
    let files_path = Path::new(app_web_files_dir);

    App::new()
      // enable TracingLogger
      .wrap(TracingLogger)
      .data(web::JsonConfig::default().limit(4096)) // <- limit size of the payload (global configuration)
      .service(
        web::resource("/json").route(web::get().to(index_json))
      )
      .service(
        web::resource("/text").route(web::get().to(index_text))
      )
      .service(
        fs::Files::new("/static", files_path).show_files_listing()
      )
      .service(
        web::resource("/").route(web::get().to(index_web))
      )
  };

  let server_host: String = get_env_or_default("APP_SERVER_HOST", "127.0.0.1");
  let server_port: usize = get_env_or_default("APP_SERVER_PORT", "1337");
  let server_workers: usize = get_env_or_default("APP_SERVER_WORKERS", "8");

  let server_address = format!("{}:{}", server_host, server_port);
  info!("server listening on 'http://{}'...", server_address);

  HttpServer::new(app)
    .workers(server_workers)
    .bind(server_address)?
    .run()
    .await
}

#[cfg(test)]
mod tests {
  use std::net::{IpAddr, Ipv4Addr, SocketAddr};

  use actix_web::{App, http, test, web};
  use actix_web::dev::Service;

  use super::*;

  #[actix_rt::test]
  async fn test_index() -> Result<(), Error> {
    let mut app = test::init_service(
      App::new()
        .service(web::resource("/").route(web::get().to(index_text)))
    ).await;
    let test_address = SocketAddr::new(
      IpAddr::V4(Ipv4Addr::new(1, 3, 3, 7)),
      8080,
    );

    let req = test::TestRequest::get()
      .peer_addr(test_address)
      .uri("/")
      .to_request();
    let resp = app.call(req).await.unwrap();
    assert_eq!(resp.status(), http::StatusCode::OK);

    let response_body: web::Bytes = test::read_body(resp).await;
    assert_eq!(response_body, web::Bytes::from(&b"\"1.3.3.7\""[..]));

    Ok(())
  }
}
