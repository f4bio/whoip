extern crate derive_more;

use std::fmt;

use actix_web::{dev::HttpResponseBuilder, error, http::header, http::StatusCode, HttpResponse};
use derive_more::Error;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct FlashMessage {
  pub kind: String,
  pub text: String,
  pub color: String,
}

/// CountryInfo
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct CountryInfo {
  pub name: String,
  pub code: String,
}

/// PostalInfo
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct PostalInfo {
  pub code: String,
}

/// CityInfo
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct CityInfo {
  pub name: String,
}

/// Coordinates
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct CoordinatesInfo {
  pub latitude: String,
  pub longitude: String,
}

/// ConnectionInfo
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ConnectionInfo {
  pub isp: String,
  // pub connection_type: String,
  // pub is_anonymous: bool,
  // pub is_anonymous_vpn: bool,
  // pub is_hosting_provider: bool,
  // pub is_public_proxy: bool,
  // pub is_tor_exit_node: bool,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct IpInfoQueryObj {
  pub address: Option<String>,
  pub language: Option<String>,
}

#[derive(Debug, Error)]
pub enum IpInfoError {
  InternalError,
  LookupError
}

impl fmt::Display for IpInfoError {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(f, "An internal error occurred. Please try again later.")
  }
}

impl error::ResponseError for IpInfoError {
  fn status_code(&self) -> StatusCode {
    match *self {
      IpInfoError::InternalError => StatusCode::INTERNAL_SERVER_ERROR,
      IpInfoError::LookupError => StatusCode::BAD_REQUEST
    }
  }
  fn error_response(&self) -> HttpResponse {
    HttpResponseBuilder::new(self.status_code())
      .set_header(header::CONTENT_TYPE, "text/html; charset=utf-8")
      .body(self.to_string())
  }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct IpInfoObj {
  pub address: String,
  pub country: Option<CountryInfo>,
  pub city: Option<CityInfo>,
  pub postal: Option<PostalInfo>,
  pub coordinates: Option<CoordinatesInfo>,
  pub connection: Option<ConnectionInfo>,
}

impl fmt::Display for IpInfoObj {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(f, "hello there! your IP is {}, you live in {}({})",
           self.address.clone(),
           self.city.clone().unwrap().name,
           self.country.clone().unwrap().name,
    )
  }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct IpInfoResponseObj {
  pub message: Option<FlashMessage>,
  pub language: String,
  pub info: IpInfoObj
}
