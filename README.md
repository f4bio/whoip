# whoip

[![Build Status](http://drone.private/api/badges/f4bio/whoip/status.svg)](http://drone.private/f4bio/whoip)

## data

### geoip

#### City

```
City {
  city: None,
  continent: Some(<Continent>), 
  country: Some(<Country>), 
  location: Some(<Country>), 
  postal: None, 
  registered_country: Some(<Country>), 
  represented_country: None, 
  subdivisions: None, 
  traits: None 
}
```

#### Continent

```
Continent {
    code: Some("OC"), 
    geoname_id: Some(6255151), 
    names: Some(
      {
        "de": "Ozeanien", 
        "en": "Oceania", 
        "es": "Oceanía", 
        "fr": "Océanie", 
        "ja": "オセアニア", 
        "pt-BR": "Oceania", 
        "ru": "Океания", 
        "zh-CN": "大洋洲"
      }
    )
  }
```

#### Country

```
Country { 
  geoname_id: Some(2077456), 
  is_in_european_union: None, 
  iso_code: Some("AU"), 
  names: Some(
    {
      "de": "Australien", 
      "en": "Australia", 
      "es": "Australia", 
      "fr": "Australie", 
      "ja": "オーストラリア", 
      "pt-BR": "Austrália", 
      "ru": "Австралия", 
      "zh-CN": "澳大利亚"
    }
  )
}
```

#### Location

```
Location { 
  latitude: Some(-33.494), 
  longitude: Some(143.2104), 
  metro_code: None, 
  time_zone: Some("Australia/Sydney") 
}
```
