const path = require("path");
const CopyPlugin = require("copy-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const FaviconsWebpackPlugin = require("favicons-webpack-plugin");
const ESLintPlugin = require("eslint-webpack-plugin");

module.exports = {
  entry: "./web/bootstrap.js",
  output: {
    path: path.resolve(__dirname, "web", "dist"),
    filename: "[name].[fullhash].js",
    publicPath: "/static/",
  },
  module: {
    rules: [
      {
        test: /\.s[ac]ss$/i,
        use: [
          "style-loader",
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              esModule: false
            }
          },
          { loader: "css-loader", options: { sourceMap: true } },
          { loader: "sass-loader", options: { sourceMap: true } },
          "postcss-loader"
        ]
      },
      {
        test: /\.(png|jpe?g|webp|git|svg|)$/i,
        use: [
          { loader: "img-optimize-loader" }
        ]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        loader: "file-loader",
        options: {
          outputPath: "fonts"
        }
      }
    ]
  },
  plugins: [
    new ESLintPlugin(),
    new FaviconsWebpackPlugin("./static/images/whoip.png"),
    new CopyPlugin({
      patterns: [
        { from: "./templates/about.html" },
        { from: "./templates/index.html" },
        // { from: "../node_modules/@fortawesome/fontawesome-free/webfonts/" },
        { from: "./static/images/" }
      ],
    }),
    new MiniCssExtractPlugin()
  ],
};
